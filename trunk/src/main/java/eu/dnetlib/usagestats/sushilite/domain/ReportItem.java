package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class ReportItem {
    private List<ItemIdentifier> itemIdentifiers = new ArrayList<>();

    private String itemPublisher;
    private String itemPlatform;
    private String itemDataType;
    private String itemName;

    private List<ItemPerformance> itemPerformances = new ArrayList<>();

    public ReportItem() {
    }

    public ReportItem(String itemPublisher, String itemPlatform, String itemDataType, String itemName) {
        this.itemPublisher = itemPublisher;
        this.itemPlatform = itemPlatform;
        this.itemDataType = itemDataType;
        this.itemName = itemName;
    }

    @JsonProperty("ItemIdentifier")
    public List<ItemIdentifier> getItemIdentifiers() {
        return itemIdentifiers;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("ItemPublisher")
    public String getItemPublisher() {
        return itemPublisher;
    }

    @JsonProperty("ItemPlatform")
    public String getItemPlatform() {
        return itemPlatform;
    }

    @JsonProperty("ItemDataType")
    public String getItemDataType() {
        return itemDataType;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("ItemName")
    public String getItemName() {
        return itemName;
    }

    @JsonProperty("ItemPerformance")
    public List<ItemPerformance> getItemPerformances() {
        return itemPerformances;
    }

    public void addIdentifier(ItemIdentifier itemIdentifier) {
        itemIdentifiers.add(itemIdentifier);
    }

    public void addPerformance(ItemPerformance itemPerformance) {
        itemPerformances.add(itemPerformance);
    }

    public void setItemPlatform(String itemPlatform) {
        this.itemPlatform = itemPlatform;
    }
}
