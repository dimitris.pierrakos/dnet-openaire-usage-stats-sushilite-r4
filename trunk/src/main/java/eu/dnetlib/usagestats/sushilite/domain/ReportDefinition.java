package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 25/10/2016.
 */
public class ReportDefinition {
    private String name;
    private String release;
    private Filters filters;

    public ReportDefinition() {
    }

    public ReportDefinition(String name, String release, String beginDate, String endDate,
                            String repositoryIdentifier, String itemIdentifier, String itemDataType, String granularity, String reportItemCount) {
        this.name = name;
        this.release = release;

        filters = new Filters(beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, granularity, reportItemCount);
    }

    @JsonProperty("@Name")
    public String getName() {
        return name;
    }

    @JsonProperty("@Release")
    public String getRelease() {
        return release;
    }

    @JsonProperty("Filters")
    public Filters getFilters() {
        return filters;
    }

}
