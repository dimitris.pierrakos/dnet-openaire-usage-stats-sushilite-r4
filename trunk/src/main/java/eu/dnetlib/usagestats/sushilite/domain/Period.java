package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class Period {
    private String begin;
    private String end;

    public Period() {
    }

    public Period(String begin, String end) {
        this.begin = begin;
        this.end = end;
    }

    @JsonProperty("Begin")
    public String getBegin() {
        return begin;
    }

    @JsonProperty("End")
    public String getEnd() {
        return end;
    }
}
