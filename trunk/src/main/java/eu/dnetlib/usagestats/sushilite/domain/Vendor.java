package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class Vendor {

    private Contact contact = new Contact();

    public Vendor() {
    }

    @JsonProperty("Name")
    public String getName() {
        return "OpenAIRE";
    }

    @JsonProperty("Contact")
    public Contact getContact() {
        return contact;
    }
}
