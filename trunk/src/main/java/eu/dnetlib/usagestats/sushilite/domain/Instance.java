package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class Instance {
    private String metricType;
    private String count;

    public Instance() {
    }

    public Instance(String type, String count) {
        this.metricType = type;
        this.count = count;
    }

    @JsonProperty("MetricType")
    public String getMetricType() {
        return metricType;
    }

    @JsonProperty("Count")
    public String getCount() {
        return count;
    }
}
