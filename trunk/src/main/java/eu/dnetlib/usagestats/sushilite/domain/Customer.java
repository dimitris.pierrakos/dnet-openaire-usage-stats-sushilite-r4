package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class Customer {
    private String id = null;

    public void setReportItems(List<ReportItem> reportItems) {
        this.reportItems = reportItems;
    }

    private List<ReportItem> reportItems = new ArrayList<>();

    public Customer() {
    }

    public Customer(String id, List<ReportItem> reportItems) {
        this.id = id;
        this.reportItems = reportItems;
    }

    @JsonProperty("ID")
    public String getId() {
        return id;
    }

    @JsonProperty("ReportItems")
    public List<ReportItem> getReportItems() {
        return reportItems;
    }
}
