package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class ReportResponseWrapper {
    private ReportResponse reportResponse;

    public ReportResponseWrapper() {}

    public ReportResponseWrapper(ReportResponse reportResponse) {
        this.reportResponse = reportResponse;
    }

    @JsonProperty("ReportResponse")
    public ReportResponse getReportResponse() {
        return reportResponse;
    }

    public void setReportResponse(ReportResponse reportResponse) {
        this.reportResponse = reportResponse;
    }
}
