package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tsampikos on 23/11/2016.
 */
public class ReportException {
    private String created;
    private String number;
    private String severity;
    private String message;
    private String data;

    public ReportException() {
    }

    public ReportException(String number, String severity, String message, String data) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        Date date = new Date();
        created = dateFormat.format(date);

        this.number = number;
        this.severity = severity;
        this.message = message;
        this.data = data;
    }

    @JsonProperty("@Created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("Number")
    public String getNumber() {
        return number;
    }

    @JsonProperty("Severity")
    public String getSeverity() {
        return severity;
    }

    @JsonProperty("Message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("Data")
    public String getData() {
        return data;
    }
}
