package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class Contact {

    @JsonProperty("Contact")
    public String getContact() {
        return "OpenAIRE Helpdesk";
    }

    @JsonProperty("E-mail")
    public String getEmail() {
        return "helpdesk@openaire.eu";
    }

    public Contact() {
    }
}
