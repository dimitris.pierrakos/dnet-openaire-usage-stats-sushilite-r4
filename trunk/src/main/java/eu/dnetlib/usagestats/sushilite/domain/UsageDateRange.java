package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class UsageDateRange {
    private String begin;
    private String end;

    public UsageDateRange() {
    }

    public UsageDateRange(String begin, String end) {
        this.begin = begin;
        this.end = end;
    }

    @JsonProperty("Begin")
    public String getBegin() {
        return begin;
    }

    @JsonProperty("End")
    public String getEnd() {
        return end;
    }
}
