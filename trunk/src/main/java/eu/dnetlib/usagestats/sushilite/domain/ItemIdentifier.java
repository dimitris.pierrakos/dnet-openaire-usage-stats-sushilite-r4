package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class ItemIdentifier {
    private String type;
    private String value;

    public ItemIdentifier() {
    }

    public ItemIdentifier(String type, String value) {
        this.type = type;
        this.value = value;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }
}
