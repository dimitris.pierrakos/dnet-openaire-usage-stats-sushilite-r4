package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tsampikos on 25/10/2016.
 */
public class ReportResponse {

    private String created;
    private List<ReportException> reportExceptions = new ArrayList<>();
    private Requestor requestor;
    private ReportDefinition reportDefinition;
    private ReportWrapper reportWrapper;

    public ReportResponse() {
    }

    public ReportResponse(String reportName, String release, String requestorId, String beginDate,
                          String endDate, String repositoryIdentifier, String itemIdentifier,
                          String itemDataType, String hasDoi, String granularity, String callback, List<ReportItem> reportItems, List<ReportException> reportExceptions) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        Date date = new Date();
        created = dateFormat.format(date);

        if (!reportExceptions.isEmpty()) {
            this.reportExceptions = reportExceptions;
        }

        requestor = new Requestor(requestorId);

        reportDefinition = new ReportDefinition(reportName, release, beginDate, endDate, repositoryIdentifier, itemIdentifier, itemDataType, granularity, Integer.toString(reportItems.size()));

        Report report = new Report(created, reportName, release, requestorId, reportItems);
        reportWrapper = new ReportWrapper(report);
    }

    @JsonProperty("@Created")
    public String getCreated() {
        return created;
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Exception")
    public List<ReportException> getReportExceptions() {
        return reportExceptions;
    }

    @JsonProperty("Requestor")
    public Requestor getRequestor() {
        return requestor;
    }

    @JsonProperty("ReportDefinition")
    public ReportDefinition getReportDefinition() {
        return reportDefinition;
    }

    @JsonProperty("Report")
    public ReportWrapper getReportWrapper() {
        return reportWrapper;
    }

}
