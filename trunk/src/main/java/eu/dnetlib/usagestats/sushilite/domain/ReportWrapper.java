package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class ReportWrapper {
    private Report report;

    public ReportWrapper() {
    }

    public ReportWrapper(Report report) {
        this.report = report;
    }

    @JsonProperty("Report")
    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }
}
