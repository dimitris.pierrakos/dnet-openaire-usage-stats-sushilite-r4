package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class Filters {
    private UsageDateRange usageDateRange = null;
    private List<Filter> filter = new ArrayList<>();
    private List<Filter> reportAttribute = new ArrayList<>();

    public Filters() {
    }

    public Filters(String beginDate, String endDate, String repositoryIdentifier,
                   String itemIdentifier, String itemDataType, String granularity, String reportItemCount) {
        usageDateRange = new UsageDateRange(beginDate, endDate);

        if (repositoryIdentifier != null && !repositoryIdentifier.equals("")) {
            filter.add(new Filter("RepositoryIdentifier", repositoryIdentifier));
        }
        if (itemIdentifier != null && !itemIdentifier.equals("")) {
            filter.add(new Filter("ItemIdentifier", itemIdentifier));
        }
        if (itemDataType != null && !itemDataType.equals("")) {
            filter.add(new Filter("ItemDataType", itemDataType));
        }

        reportAttribute.add(new Filter("Granularity", granularity));
        if (reportItemCount != null && !reportItemCount.equals("")) {
            reportAttribute.add(new Filter("ReportItemCount", reportItemCount));
        }
    }

    @JsonProperty("UsageDateRange")
    public UsageDateRange getUsageDateRange() {
        return usageDateRange;
    }

    @JsonProperty("Filter")
    public List<Filter> getFilter() {
        return filter;
    }

    @JsonProperty("ReportAttribute")
    public List<Filter> getReportAttribute() {
        return reportAttribute;
    }
}
