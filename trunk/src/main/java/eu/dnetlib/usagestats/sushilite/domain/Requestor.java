package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 25/10/2016.
 */
public class Requestor {
    private String id;

    public Requestor() {
    }

    public Requestor(String id) {
        this.id = id;
    }

    @JsonProperty("ID")
    public String getID() {
        return id;
    }

}
